/* Load libraries. */
var fs = require('fs')
var lunr = require('lunr')

/* Read the index (+ metadata) file and parse it. */
var contents = fs.readFileSync('book/build/site/search_index.json', 'utf8')
var parsed = JSON.parse(contents)

/* Extract the index and metadata. */
var idx = lunr.Index.load(parsed.index)
var url = parsed.url
var store = parsed.store

/* Do a test search. */
var search_string = 'number'

var result = idx.search(search_string)

var output = []

function highlightText (doc, position) {
    var hits = []
    var start = position[0]
    var length = position[1]

    var text = doc.body
    var highlightSpan = '<span class="search-result-highlight">' + text.substr(start, length) + '</span>'

    var end = start + length
    var textEnd = text.length - 1
    var contextOffset = 15
    var contextAfter = end + contextOffset > textEnd ? textEnd : end + contextOffset
    var contextBefore = start - contextOffset < 0 ? 0 : start - contextOffset
    if (start === 0 && end === textEnd) {
        hits.push(highlightSpan)
    } else if (start === 0) {
        hits.push(highlightSpan)
        hits.push(text.substr(end, contextAfter))
    } else if (end === textEnd) {
        hits.push(text.substr(0, start))
        hits.push(highlightSpan)
    } else {
        hits.push('...' + text.substr(contextBefore, start - contextBefore))
        hits.push(highlightSpan)
        hits.push(text.substr(end, contextAfter - end) + '...')
    }
    return hits
}

function highlightTitle (doc, position) {
    var hits = []
    var start = position[0]
    var length = position[1]

    var title = doc.title
    var highlightSpan = '<span class="search-result-highlight">' + title.substr(start, length) + '</span>'

    var end = start + length
    var titleEnd = title.length - 1
    if (start === 0 && end === titleEnd) {
        hits.push(highlightSpan)
    } else if (start === 0) {
        hits.push(highlightSpan)
        hits.push(title.substr(length, titleEnd))
    } else if (end === titleEnd) {
        hits.push(title.substr(0, start))
        hits.push(highlightSpan)
    } else {
        hits.push(title.substr(0, start))
        hits.push(highlightSpan)
        hits.push(title.substr(end, titleEnd))
    }
    return hits
}

function highlightHit (metadata, doc) {
    var hits = []
    for (var token in metadata) {
        var fields = metadata[token]
        for (var field in fields) {
            var positions = fields[field]
            if (positions.position) {
                var position = positions.position[0] // only highlight the first match
                if (field === 'title') {
                    hits = highlightTitle(doc, position)
                } else if (field === 'body') {
                    hits = highlightText(doc, position)
                }
            }
        }
    }
    return hits
}

function createSearchResultItem (doc, docurl, hits) {
    var result = '<div class="search-result-item">\n'
    result += '  <div class="search-result-document-title">' + doc.title + '</div>\n'

    result += '  <div class="search-result-document-hit">\n'
    result += '    <a href="' + docurl + '">\n'

    hits.forEach(function (hit) {
        result += '      ' + hit + '\n'
    })

    result += '    </a>\n'

    result += '  </div>\n'
    result += '</div>\n'

    return result
}

if (result.length > 0) {
    result.forEach(function (item) {
        var doc = store[item.ref]
        var docurl = url + store[item.ref].loc
        var metadata = item.matchData.metadata

        var hits = highlightHit(metadata, doc)

        /*
        console.log('')
        console.log(docurl)
        console.log(hits)
        */
        console.log(createSearchResultItem(doc, docurl, hits))


        //output.push(createSearchResultItem(doc, docurl, hits))
    })
} else {
    console.log('no hits')
}



/*
console.log(result)

var ref = result[0].ref

var docurl = url + store[ref].loc
var doctitle = store[ref].title
var docbody = store[ref].body

console.log(doctitle)
console.log(docurl)
console.log('')
console.log(docbody)

console.log('')
console.log(result[0].matchData.metadata[search_string]['body']['position'])
*/

